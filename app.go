package main

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/andlabs/ui"
)

type App struct {
	window *ui.Window
	grid   *ui.Grid

	apiKeyEntry      *ui.Entry
	secretKeyEntry   *ui.Entry
	useProxyCheckbox *ui.Checkbox
	proxyEntry       *ui.Entry
	messageLabel     *ui.Label
	startButton      *ui.Button

	configFName string
	config      Config
}

func (a *App) log(message string) {
	log.Println(message)

	ui.QueueMain(func() { a.messageLabel.SetText(message) })
}

func (a *App) error(err error) {
	a.log(fmt.Sprintf("Ошибка: %s", err.Error()))
}

func (a *App) start(_ *ui.Button) {
	logic, err := NewLogic(&a.config)
	if err != nil {
		ui.MsgBoxError(a.window, "Ошибка", fmt.Sprint(err))
		return
	}

	filename := ui.OpenFile(a.window)
	if filename == "" {
		return
	}

	a.disableUI()

	filename = filepath.ToSlash(filename)

	msgs := make(chan string)
	errs := make(chan error)

	a.log("Старт")

	go logic.Run(filename, msgs, errs)

	go func() {
		defer a.enableUI()

		for {
			select {
			case msg, ok := <-msgs:
				if !ok {
					a.log("Готово")
					return
				}

				a.log(msg)
			case err := <-errs:
				switch err.(type) {
				case *AddressGetterError:
					a.error(err)
				default:
					// print error and stop
					a.error(err)
					return
				}
			}
		}
	}()
}

func (a *App) initConfig(configFName string) {
	a.config = Config{}
	a.configFName = configFName

	a.config.Read(a.configFName)
}

func (a *App) writeConfig() {
	a.config.Write(a.configFName)
}

func (a *App) initWindow() {
	a.window = ui.NewWindow("dadata batch", 640, 480, true)

	a.window.SetMargined(true)
	a.window.OnClosing(func(*ui.Window) bool {
		ui.Quit()
		return true
	})

	ui.OnShouldQuit(func() bool {
		a.window.Destroy()
		return true
	})
}

func (a *App) initGrid() {
	a.grid = ui.NewGrid()
	a.grid.SetPadded(true)

	a.window.SetChild(a.grid)
}

// nolint:dupl // this is NOT duplicate
func (a *App) initAPIKeyEntry() {
	entry := ui.NewEntry()

	entry.SetText(a.config.APIKey)
	entry.OnChanged(func(e *ui.Entry) {
		a.config.APIKey = e.Text()
		a.writeConfig()
	})

	a.grid.Append(ui.NewLabel("API key"), 0, 0, 1, 1, false, ui.AlignFill, false, ui.AlignFill)
	a.grid.Append(entry, 1, 0, 1, 1, true, ui.AlignFill, false, ui.AlignFill)

	a.apiKeyEntry = entry
}

// nolint:dupl // this is NOT duplicate
func (a *App) initSecretKeyEntry() {
	entry := ui.NewEntry()

	entry.SetText(a.config.SecretKey)
	entry.OnChanged(func(e *ui.Entry) {
		a.config.SecretKey = e.Text()
		a.writeConfig()
	})

	a.grid.Append(ui.NewLabel("Secret key"), 0, 1, 1, 1, false, ui.AlignFill, false, ui.AlignFill)
	a.grid.Append(entry, 1, 1, 1, 1, true, ui.AlignFill, false, ui.AlignFill)

	a.secretKeyEntry = entry
}

func (a *App) initProxyEntry() {
	entry := ui.NewPasswordEntry()

	entry.SetText(a.config.ProxyURL)
	entry.OnChanged(func(e *ui.Entry) {
		a.config.ProxyURL = e.Text()
		a.writeConfig()
	})

	a.grid.Append(entry, 0, 4, 2, 1, false, ui.AlignFill, false, ui.AlignFill)

	a.proxyEntry = entry
}

func (a *App) initUseProxyCheckBox() {
	checkbox := ui.NewCheckbox("Использовать прокси (Если URL пустой, будет использована HTTPS_PROXY или HTTP_PROXY)")
	checkbox.SetChecked(a.config.UseProxy)

	if checkbox.Checked() {
		a.proxyEntry.Show()
	} else {
		a.proxyEntry.Hide()
	}

	checkbox.OnToggled(func(c *ui.Checkbox) {
		if c.Checked() {
			a.proxyEntry.Show()
		} else {
			a.proxyEntry.Hide()
		}

		a.config.UseProxy = c.Checked()
		a.writeConfig()
	})

	a.grid.Append(checkbox, 0, 3, 2, 1, false, ui.AlignFill, false, ui.AlignFill)

	a.useProxyCheckbox = checkbox
}

func (a *App) initMessageLabel() {
	a.messageLabel = ui.NewLabel("Лог")
	a.grid.Append(a.messageLabel, 0, 5, 2, 1, true, ui.AlignFill, true, ui.AlignEnd)
}

func (a *App) initStartButton() {
	button := ui.NewButton("Старт")

	button.OnClicked(a.start)

	a.grid.Append(button, 0, 2, 2, 1, true, ui.AlignFill, false, ui.AlignFill)

	a.startButton = button
}

func (a *App) disableUI() {
	ui.QueueMain(func() {
		a.apiKeyEntry.Disable()
		a.secretKeyEntry.Disable()
		a.useProxyCheckbox.Disable()
		a.proxyEntry.Disable()
		a.startButton.Disable()
	})
}

func (a *App) enableUI() {
	ui.QueueMain(func() {
		a.apiKeyEntry.Enable()
		a.secretKeyEntry.Enable()
		a.useProxyCheckbox.Enable()
		a.proxyEntry.Enable()
		a.startButton.Enable()
	})
}

func (a *App) init(configFName string) {
	a.initConfig(configFName)

	a.initWindow()
	a.initGrid()

	a.initAPIKeyEntry()
	a.initSecretKeyEntry()
	a.initProxyEntry()
	a.initUseProxyCheckBox()
	a.initMessageLabel()
	a.initStartButton()
}

func (a *App) Run() {
	a.init(a.configFName)
	a.window.Show()
}

func NewApp(configFName string) *App {
	return &App{configFName: configFName}
}
