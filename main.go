package main

import (
	"flag"
	"log"

	// ui
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
)

func main() {
	configFName := flag.String("config", "", "config file name [default - {Home Dir}/"+ConfigName)
	flag.Parse()

	app := NewApp(*configFName)

	err := ui.Main(app.Run)
	if err != nil {
		log.Fatal(err)
	}
}
