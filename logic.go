package main

import "fmt"

type Logic struct {
	AddressGetter AddressGetter

	QueryGetter  QueryGetter
	ResultWriter ResultWriter
}

func (l *Logic) setFile(fname string) error {
	l.QueryGetter = nil
	l.ResultWriter = nil

	qg, err := NewXLSXQueryGetter(fname)
	if err != nil {
		return err
	}

	rw, err := NewXLSXResultWrite(fname)
	if err != nil {
		return err
	}

	l.QueryGetter = qg
	l.ResultWriter = rw

	return nil
}

func (l *Logic) writeAddressInfoErrors(addressInfos []AddressInfo, errs chan<- error) {
	for i := range addressInfos {
		addressInfo := &addressInfos[i]

		if addressInfo.Raw.Error != nil {
			errs <- addressInfo.Raw.Error
		}

		if addressInfo.Street != nil && addressInfo.Street.Error != nil {
			errs <- addressInfo.Street.Error
		}

		if addressInfo.Settlement != nil && addressInfo.Settlement.Error != nil {
			errs <- addressInfo.Settlement.Error
		}
	}
}

func (l *Logic) Run(fname string, msgs chan<- string, errs chan<- error) {
	defer func() {
		close(msgs)
	}()

	err := l.setFile(fname)
	if err != nil {
		errs <- err
		return
	}

	err = l.QueryGetter.Run(func(query *Query) error {
		msgs <- fmt.Sprint(query)

		addressInfos := l.AddressGetter.ByQuery(query)

		l.writeAddressInfoErrors(addressInfos, errs)

		return l.ResultWriter.Write(query, addressInfos)
	})

	if err != nil {
		errs <- err
		return
	}

	err = l.ResultWriter.Close()
	if err != nil {
		errs <- err
		return
	}
}

func NewLogic(config *Config) (*Logic, error) {
	ag, err := NewDadataAddressGetter(config)
	if err != nil {
		return nil, err
	}

	return &Logic{AddressGetter: ag}, nil
}
