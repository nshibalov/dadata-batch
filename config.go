package main

import (
	"io/ioutil"
	"log"
	"os"
	"os/user"
	"path"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

const (
	ConfigName            = ".dadata-batch.yaml"
	DefaultMaxSuggestions = 3
)

type Config struct {
	APIKey    string `yaml:"api_key"`
	SecretKey string `yaml:"secret_key"`

	ProxyURL string `yaml:"proxy_url"`
	UseProxy bool   `yaml:"use_proxy"`

	MaxSuggestions int `yaml:"max_suggestions"`
}

func (Config) getDefaultConfig() string {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}

	return path.Join(usr.HomeDir, ConfigName)
}

func (c *Config) Read(fname string) {
	if fname == "" {
		fname = c.getDefaultConfig()
	} else {
		fname = filepath.ToSlash(fname)
	}

	if _, err := os.Stat(fname); os.IsNotExist(err) {
		c.Write(fname)
	}

	yamlFile, err := ioutil.ReadFile(fname)
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		log.Fatal(err)
	}

	if c.MaxSuggestions == 0 {
		c.MaxSuggestions = DefaultMaxSuggestions
	}
}

func (c *Config) Write(fname string) {
	if fname == "" {
		fname = c.getDefaultConfig()
	}

	data, err := yaml.Marshal(c)
	if err != nil {
		log.Fatal(err)
	}

	if err := ioutil.WriteFile(fname, data, 0666); err != nil {
		log.Fatal(err)
	}
}

func (c *Config) GetProxyURL() string {
	if c.ProxyURL != "" {
		return c.ProxyURL
	}

	proxyURL := os.Getenv("HTTPS_PROXY")
	if proxyURL == "" {
		return os.Getenv("HTTP_PROXY")
	}

	return proxyURL
}
