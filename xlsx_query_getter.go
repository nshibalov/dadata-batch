package main

import "github.com/tealeg/xlsx"

type xlsxQueryGetter struct {
	file *xlsx.File
}

func (g xlsxQueryGetter) Run(callback func(*Query) error) error {
	for _, sheet := range g.file.Sheets {
		total := len(sheet.Rows)

		for rowID, row := range sheet.Rows {
			if rowID == 0 {
				continue // skip header
			}

			if len(row.Cells) < 1 {
				continue
			}

			err := callback(&Query{
				RowID:       rowID,
				Total:       total,
				Sheet:       sheet.Name,
				QueryString: row.Cells[0].String(),
			})

			if err != nil {
				return err
			}
		}
	}

	return nil
}

func NewXLSXQueryGetter(fname string) (QueryGetter, error) {
	xlFile, err := xlsx.OpenFile(fname)
	if err != nil {
		return nil, err
	}

	return &xlsxQueryGetter{
		file: xlFile,
	}, nil
}
