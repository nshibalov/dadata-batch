package main

import "fmt"

type AddressGetterError struct {
	InfoType string
	Message  string
	Query    *Query
}

func (e *AddressGetterError) Error() string {
	return fmt.Sprintf("Ошибка при получении адреса (%s): %s", e.InfoType, e.Message)
}
