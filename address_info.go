package main

type AddressInfo struct {
	Raw Address

	Street,
	Settlement *Address
}
