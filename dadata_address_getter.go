package main

import (
	"errors"
	"net/http"
	"net/url"
	"regexp"
	"strconv"

	"github.com/google/uuid"
	"gopkg.in/webdeskltd/dadata.v2"
)

const (
	reLatLonCount = 2
)

// nolint:gochecknoglobals // ok for regexp
var reLatLonSep = regexp.MustCompile(`[;:, ]+`)

type dadataAddressGetter struct {
	config       *Config
	dadataCli    *dadata.DaData
	addressCache map[string]*Address
}

func (g *dadataAddressGetter) byID(fiasID string) (*Address, error) {
	if fiasID == "" {
		return nil, nil
	}

	if addr, ok := g.addressCache[fiasID]; ok {
		return addr, nil
	}

	address, err := g.dadataCli.AddressByID(fiasID)
	if err != nil {
		return nil, err
	}

	addr := &Address{
		ID:    address.Data.AreaFiasID,
		Value: address.Value,
	}

	g.addressCache[fiasID] = addr

	return addr, nil
}

func (g *dadataAddressGetter) byUUIDQuery(query *Query) (bool, []AddressInfo) {
	id, err := uuid.Parse(query.QueryString)
	if err != nil {
		return false, nil
	}

	address, err := g.byID(id.String())
	if err != nil {
		return true, []AddressInfo{{Raw: Address{
			Error: &AddressGetterError{InfoType: "По UUID", Message: err.Error()},
		}}}
	}

	return true, []AddressInfo{{Raw: *address}}
}

func (g *dadataAddressGetter) byGeoLocQuery(query *Query) (bool, []AddressInfo) {
	parts := reLatLonSep.Split(query.QueryString, -1)

	if len(parts) != reLatLonCount {
		return false, nil
	}

	lat, err := strconv.ParseFloat(parts[0], 32)
	if err != nil {
		return false, nil
	}

	lon, err := strconv.ParseFloat(parts[1], 32)
	if err != nil {
		return false, nil
	}

	addresses, err := g.dadataCli.GeolocateAddress(dadata.GeolocateRequest{
		Lat:   float32(lat),
		Lon:   float32(lon),
		Count: g.config.MaxSuggestions,
	})

	if err != nil {
		return true, []AddressInfo{{Raw: Address{
			Error: &AddressGetterError{InfoType: "По координатам", Message: err.Error()},
		}}}
	}

	ret := make([]AddressInfo, len(addresses))

	for i := range addresses {
		address := &addresses[i]

		ret[i].Raw = Address{
			ID:    address.Data.FiasID,
			Value: address.Value,
		}
	}

	return true, ret
}

func (g *dadataAddressGetter) ByQuery(query *Query) []AddressInfo {
	ok, ret := g.byUUIDQuery(query)
	if ok {
		return ret
	}

	ok, ret = g.byGeoLocQuery(query)
	if ok {
		return ret
	}

	// untyped suggest

	addresses, err := g.dadataCli.SuggestAddresses(dadata.SuggestRequestParams{
		Query: query.QueryString,
		Count: g.config.MaxSuggestions,
	})

	if err != nil {
		return []AddressInfo{{Raw: Address{
			Error: &AddressGetterError{InfoType: "Подбор адреса", Message: err.Error()},
		}}}
	}

	ret = make([]AddressInfo, 0, len(addresses))

	for i := range addresses {
		address := &addresses[i]

		info := AddressInfo{
			Raw: Address{ID: address.Data.FiasID, Value: address.Value},
		}

		street, err := g.byID(address.Data.StreetFiasID)
		if err != nil {
			info.Street = &Address{
				Error: &AddressGetterError{InfoType: "Адрес улицы", Message: err.Error()},
			}
		} else {
			info.Street = street
		}

		settlement, err := g.byID(address.Data.SettlementFiasID)
		if err != nil {
			info.Street = &Address{
				Error: &AddressGetterError{InfoType: "Адрес поселения", Message: err.Error()},
			}
		} else {
			info.Settlement = settlement
		}

		ret = append(ret, info)
	}

	return ret
}

func NewDadataAddressGetter(config *Config) (AddressGetter, error) {
	if config.APIKey == "" {
		return nil, errors.New("не указан API Key")
	} else if config.SecretKey == "" {
		return nil, errors.New("не указан Secret Key")
	}

	var daData *dadata.DaData

	proxyURLStr := config.GetProxyURL()
	if config.UseProxy && proxyURLStr != "" {
		proxyURL, err := url.Parse(proxyURLStr)
		if err != nil {
			return nil, err
		}

		client := &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyURL(proxyURL),
			},
		}

		daData = dadata.NewDaDataCustomClient(config.APIKey, config.SecretKey, client)
	} else {
		daData = dadata.NewDaData(config.APIKey, config.SecretKey)
	}

	return &dadataAddressGetter{
		config:       config,
		dadataCli:    daData,
		addressCache: make(map[string]*Address),
	}, nil
}
