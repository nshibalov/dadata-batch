package main

import (
	"fmt"
	"path"

	"github.com/tealeg/xlsx"
)

const (
	addrDistance = 2
)

type xlsxResultWriter struct {
	fname string
	file  *xlsx.File

	sheet *xlsx.Sheet
}

func (w *xlsxResultWriter) init() error {
	sheet, err := w.file.AddSheet("DADATA")
	if err != nil {
		return err
	}

	w.sheet = sheet

	{ // header
		header := sheet.AddRow()
		header.AddCell().Value = "Стока запроса"
		header.AddCell().Value = "Запрос"
		header.AddCell().Value = "Адрес"
		header.AddCell().Value = "ФИАСС"
		header.AddCell().Value = "Информация/Ошибка"
		header.AddCell().Value = "Адрес (Улица)"
		header.AddCell().Value = "ФИАСС (Улица)"
		header.AddCell().Value = "Ошибка (Улица)"
		header.AddCell().Value = "Адрес (Поселение)"
		header.AddCell().Value = "ФИАСС (Поселение)"
		header.AddCell().Value = "Ошибка (Поселение)"
	}

	return nil
}

func (w *xlsxResultWriter) writeAddress(row *xlsx.Row, addr *Address) bool {
	if addr == nil {
		row.AddCell().Value = "-"
		row.AddCell().Value = "-"
		row.AddCell().Value = ""

		return false
	}

	if addr.Error != nil {
		row.AddCell().Value = "-"
		row.AddCell().Value = "-"
		row.AddCell().Value = addr.Error.Error()

		return false
	}

	row.AddCell().Value = addr.Value
	row.AddCell().Value = addr.ID
	row.AddCell().Value = "-"

	return true
}

func (w *xlsxResultWriter) Write(query *Query, addresses []AddressInfo) error {
	sourceRow := w.sheet.AddRow()
	sourceRow.AddCell().Value = fmt.Sprint(query.RowID)
	sourceRow.AddCell().Value = query.QueryString

	for i := range addresses {
		addr := &addresses[i]

		var row *xlsx.Row

		if i == 0 {
			row = sourceRow
		} else {
			row = w.sheet.AddRow()
			row.AddCell().Value = fmt.Sprint(query.RowID)
			row.AddCell() // empty
		}

		row.AddCell().Value = addr.Raw.Value
		row.AddCell().Value = addr.Raw.ID

		infoCell := row.AddCell()

		if addr.Raw.Error != nil {
			infoCell.Value = addr.Raw.Error.Error()
			continue
		}

		if ok := w.writeAddress(row, addr.Street); ok {
			if len(addr.Raw.Value)-len(addr.Street.Value) >= addrDistance {
				infoCell.Value = addr.Raw.Value[len(addr.Street.Value)+addrDistance:]
			}
		}

		if ok := w.writeAddress(row, addr.Settlement); ok {
			if infoCell.Value == "" && len(addr.Raw.Value)-len(addr.Settlement.Value) >= addrDistance {
				infoCell.Value = addr.Raw.Value[len(addr.Settlement.Value)+addrDistance:]
			}
		}
	}

	return nil
}

func (w *xlsxResultWriter) Close() error {
	dir := path.Dir(w.fname)
	base := fmt.Sprintf("dadata_%s", path.Base(w.fname))

	return w.file.Save(path.Join(dir, base))
}

func NewXLSXResultWrite(fname string) (ResultWriter, error) {
	ret := &xlsxResultWriter{
		fname: fname,
		file:  xlsx.NewFile(),
	}

	err := ret.init()
	if err != nil {
		return nil, err
	}

	return ret, nil
}
