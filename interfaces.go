package main

type QueryGetter interface {
	Run(func(*Query) error) error
}

type AddressGetter interface {
	ByQuery(*Query) []AddressInfo
}

type ResultWriter interface {
	Write(*Query, []AddressInfo) error
	Close() error
}
