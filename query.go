package main

import "fmt"

const (
	hundredPerent = 100.0
)

type Query struct {
	RowID int
	Total int
	Sheet string

	QueryString string
}

func (q Query) String() string {
	rowID := q.RowID + 1
	percent := float64(rowID) / float64(q.Total) * hundredPerent

	return fmt.Sprintf("[%s] (%d/%d %.2f%%) %s", q.Sheet, rowID, q.Total, percent, q.QueryString)
}
