module dadata-batch

go 1.13

require (
	github.com/andlabs/ui v0.0.0-20200610043537-70a69d6ae31e
	github.com/google/uuid v1.1.1
	github.com/tealeg/xlsx v1.0.5
	gopkg.in/webdeskltd/dadata.v2 v2.0.0-20190503150402-ba1c2deb8492
	gopkg.in/yaml.v2 v2.3.0
)
